# Copyright 2008 Anders Ossowicki <arkanoid@exherbo.org>
# Copyright 2014 Johannes Nixdorf <mixi@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

if ever is_scm ; then
    SCM_REPOSITORY="https://git.videolan.org/git/${PN}.git"
    require scm-git
else
    MY_PNV="${PN}-snapshot-${PV}-2245-stable"
    DOWNLOADS="https://download.videolan.org/pub/videolan/${PN}/snapshots/${MY_PNV}.tar.bz2"
fi

export_exlib_phases src_prepare src_configure src_compile src_test src_install

SUMMARY="Library for encoding H264/AVC video streams"
HOMEPAGE="https://www.videolan.org/developers/${PN}.html"

LICENCES="GPL-2 GPL-3"
SLOT="0"
MYOPTIONS="
    10bit [[ description = [ set default output bit depth to 10 instead of 8 ] ]]
    visualisation [[ description = [ Visualize the encoding process ] ]]
    platform: amd64 x86
"

DEPENDENCIES="
    build:
        platform:amd64? ( dev-lang/nasm[>=2.13] )
        platform:x86? ( dev-lang/nasm[>=2.13] )
    build+run:
        visualisation? ( x11-libs/libX11 )
"

if ! ever is_scm ; then
    WORK=${WORKBASE}/${MY_PNV}
fi

bit_depths=( 10 8 )

_x264_assembler() {
    case "$(exhost --target)" in
    i686-*|x86_64-*)
        echo nasm
    ;;
    *)
        echo $(exhost --tool-prefix)cc
    ;;
    esac
}

x264_src_prepare() {
    for bit_depth in "${bit_depths[@]}"; do
        edo cp -r "${WORK}" "${WORKBASE}"/${bit_depth}
    done
}

x264_src_configure() {
    export AS=$(_x264_assembler)

    local myconf=(
        # prefixed string executable isn\'t found otherwise
        --cross-prefix=$(exhost --tool-prefix)
        --enable-shared
        --disable-avs
        --disable-ffms
        --disable-gpac
        --disable-lavf
        --disable-opencl
        --disable-swscale
    )

    # --disable-visualize doesn't exist so don't use option_enable
    option visualisation && myconf+=( "--enable-visualize" )

    for bit_depth in "${bit_depths[@]}"; do
        pushd "${WORKBASE}"/${bit_depth}
        econf "${myconf[@]}" --bit-depth="${bit_depth}"
        popd
    done
}

x264_src_compile() {
    for bit_depth in "${bit_depths[@]}"; do
        pushd "${WORKBASE}"/${bit_depth}
        emake
        popd
    done
}

x264_src_test() {
    :
}

x264_src_install() {
    local lib_name host=$(exhost --target)


    # install non-selected bit depth version prefixed
    local bit_depth=$(option 10bit "8" "10")
    pushd "${WORKBASE}"/${bit_depth}
    emake DESTDIR="${IMAGE}" install
    popd

    lib_name=$(echo "${IMAGE}"/usr/${host}/lib/libx264.so.*)
    lib_name=${lib_name#${IMAGE}/usr/${host}/lib}

    edo mkdir -p "${IMAGE}"/usr/${host}/lib/x264-${bit_depth}/
    edo mv "${IMAGE}"/usr/${host}/lib/${lib_name} \
        "${IMAGE}"/usr/${host}/lib/x264-${bit_depth}/
    dosym ${lib_name##*/} \
        /usr/${host}/lib/x264-${bit_depth}/libx264.so
    edo mv "${IMAGE}"/usr/${host}/bin/x264 \
        "${IMAGE}"/usr/${host}/bin/x264-${bit_depth}
    edo rm "${IMAGE}"/usr/${host}/lib/libx264.so


    # install main version regularly
    bit_depth=$(option 10bit "10" "8")
    pushd "${WORKBASE}"/${bit_depth}
    emake DESTDIR="${IMAGE}" install
    popd


    emagicdocs

    dodoc "${FILES}"/README.exherbo
}

